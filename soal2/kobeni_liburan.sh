#!/bin/bash


if [ $1 == "download" ]; # Argumen untuk menjalankan blok kode download gambar 
then
	hour=$(date +%H)

	if [ "$hour" = "00" ]; then
	  times=1
	else
	  times=$hour
	fi

	count=$(ls -d kumpulan_* 2>/dev/null | wc -l)
	count=$((count+1))

	mkdir kumpulan_$count

	for i in $(seq 1 $times); do
	  wget -O $(pwd)/kumpulan_$count/perjalanan_$i.jpg https://source.unsplash.com/400x400/?Indonesia
	done

elif [ $1 == "zip" ]; # Argumen untuk menjalankan blok kode zipping
then
	zipcount=$(ls -l *.zip 2>/dev/null | wc -l)
	if [ $zipcount -gt 0 ]; then
    	   zipfile="$(pwd)/devil_$(($zipcount+1)).zip"
    	   zip -r $zipfile kumpulan_*
    	   echo "Zipping kumpulan_* folders to $zipfile"
	else
	   zip -r "devil_1.zip" kumpulan_*
	fi
fi

# 0 */10 * * * /bin/bash /home/kali/Sisop-Modul-1/soal2/kobeni_liburan.sh download
# 0 0 * * * /bin/bash /home/kali/Sisop-Modul-1/soal2/kobeni_liburan.sh zip 
