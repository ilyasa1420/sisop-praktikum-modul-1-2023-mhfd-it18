 #!/bin/bash

# User mengisi username
read -p "Enter username: " username

# memeriksa apakah username sudah masuk di users.txt
if grep -q "^$username:" /home/mailyasa14/Soal3/users/users.txt; then
  echo "REGISTER: ERROR User already exists" >> log.txt
  echo "User already exists."
  exit 1
fi

# User mengisi password
while true; do
  read -sp "Enter password: " password
  echo

  # cek apakah password sudah memenuhi requirement
  if [[ ${#password} -lt 8 ]]; then
    echo "Password must be at least 8 characters long."
  elif [[ ! $password =~ [A-Z] ]]; then
    echo "Password must contain at least one uppercase letter."
  elif [[ ! $password =~ [a-z] ]]; then
    echo "Password must contain at least one lowercase letter."
  elif [[ ! $password =~ [0-9] ]]; then
    echo "Password must contain at least one digit."
  elif [[ $password == "$username" ]]; then
    echo "Password cannot contain the username."
  elif [[ $password == "chicken" || $password == "ernie" ]]; then
    echo "Password cannot contain the words 'chicken' or 'ernie'."
  else
    # menyimpan data kedalam /users/users.txt
    echo "$username:$hashed_password" >> /home/mailyasa14/Soal3/users/users.txt
    echo "REGISTER: INFO User $username registered successfully" >> log.txt
    echo "User $username registered successfully."
    exit 0
  fi
done
