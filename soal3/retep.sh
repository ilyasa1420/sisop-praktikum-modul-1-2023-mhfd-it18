#!/bin/bash

# User mengisi username username
read -p "Enter username: " username

# periksa apakah users sudah terdata di /users.txt
if ! grep -q "^$username:" /home/mailyasa14/Soal3/users/users.txt; then
  echo "User does not exist"
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
fi

# meminta users untuk mengisi password
read -p "Enter password: " password

# mendapatkan password yang tersimpan dalam /users.txt
saved_password=$(grep "^$username:" /home/mailyasa14/Soal3/users/users.txt | cut -d ":" -f 2)

# periksa apakah password sama dengan yang terimpan dalam users.txt
if [ "$password" != "$saved_password" ]; then
  echo "Incorrect password"
  echo "$(date +"%y/%m/%d %T") LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  exit 1
else
  echo "Logged in successfully"
  echo "$(date +"%y/%m/%d %T") LOGIN: INFO User $username logged in" >> /home/mailyasa14/Soal3/users/log.txt
  exit 0
fi
