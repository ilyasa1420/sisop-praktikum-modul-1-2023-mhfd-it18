#!/bin/bash

filter1=$(awk -F ',' '$3 == "JP" {print $0}' '2023 QS World University Rankings.csv' |
	 sort -t ',' -k1 -n |
	 head -n 5 |
	 awk -F ',' '{print $1,$2,$3,$4}')
echo "5 Universitas dengan ranking tertinggi di Jepang adalah sebagai berikut:"
echo "$filter1"
echo


filter2=$(awk -F ',' '$3 == "JP" {print $0}' '2023 QS World University Rankings.csv' |
	 sort -t ',' -k9 -n |
	 head -n 5 |
	 awk -F ',' '{print $2,$3,$4,$9}')

echo "5 Universitas Jepang yang memiliki FSR score paling rendah adalah:"
echo "$filter2"
echo

filter3=$(awk -F ',' '$3 == "JP" {print $0}' '2023 QS World University Rankings.csv' |
	 sort -t ',' -k20 -nr |
	 head -n 10 |
	 awk -F ',' '{print $2,$3,$4,$20}')

echo "10 Universitas di Jepang yang memiliki GER rank tertinggi:"
echo "$filter3"
echo


filter4=$(awk -F ',' '$2 ~ /Keren/ {print $2,$3,$4}' '2023 QS World University Rankings.csv')

echo "Universitas paling keren adalah:"
echo "$filter4"
