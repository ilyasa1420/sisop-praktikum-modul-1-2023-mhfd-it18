#!/bin/bash

# get current time
current_time=$(date "+%H:%M")

# get current date
current_date=$(date "+%d:%m:%Y")

# get current hour
hour=$(date "+%H")
hour=${hour#0}

# input encrypt file name
read -p "Input the name of the file to be decrypted: " filename

# define alphabet array
alphabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
capital=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# create decrypt file name with timestamp
decrypt_file="decrypt_${current_time}_${current_date}.txt"

# convert the alphabet array based on the hour
convert_alphabet=()
for (( i=0; i<26; i++ )); do
    convert_index=$(( (i + hour) % 26 ))
    convert_alphabet[i]=${alphabet[convert_index]}
done

# convert the capital array based on the hour
convert_capital=()
for (( a=0; a<26; a++ )); do
    convert_index=$(( (a + hour) % 26 ))
    convert_capital[a]=${capital[convert_index]}
done

# encrypt log data and write to decrypt file
while read -r line; do
    decrypted_data=$(echo $line | tr "${convert_alphabet[*]}" "${alphabet[*]}" | tr "${convert_capital[*]}" "${capital[*]} ")
    echo $decrypted_data >> "/home/kali/Desktop/soal4/$decrypt_file"
done < "$filename"
