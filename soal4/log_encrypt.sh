#!/bin/bash

# get current time
current_time=$(date "+%H:%M")

# get current date
current_date=$(date "+%d:%m:%Y")

# get current hour
hour=$(date "+%H")
hour=${hour#0}

# define alphabet array
alphabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
capital=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# create encrypt file name with timestamp
encrypt_file="encrypt_${current_time}_${current_date}.txt"

# convert the alphabet array based on the hour
convert_alphabet=()
for (( i=0; i<26; i++ )); do
    convert_index=$(( (i + hour) % 26 ))
    convert_alphabet[i]=${alphabet[convert_index]}
done

# convert the capital array based on the hour
convert_capital=()
for (( a=0; a<26; a++ )); do
    convert_index=$(( (a + hour) % 26 ))
    convert_capital[a]=${capital[convert_index]}
done

# encrypt log data and write to encrypt file
while read -r line; do
    encrypted_data=$(echo $line | tr "${alphabet[*]}" "${convert_alphabet[*]}" | tr "${capital[*]}" "${convert_capital[*]}")
    echo $encrypted_data >> "/home/kali/Desktop/soal4/$encrypt_file"
done < /var/log/syslog

# crontab -e
# 0 */2 * * * /home/kali/Desktop/soal4/log_encrypt.sh